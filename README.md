# Santun fish-asetukset

Tämän repositorion voi kopioida suoraan kansioksi

	$HOME/.config/fish/

Tämän jälkeen Santulle mieluiset `fish`-asetukset astuvat voimaan, kunhan
terminaali-ikkunan käynnistää uudestaan, tai suorittaa uudelleenkirjautumisen
komentoriville. Komennolla

	git clone git@gitlab.com:SeSodesa/santun-fish-asetukset.git $HOME/.config/fish

tämän saa tehtyä kaikista helpoiten.
