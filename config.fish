if status is-interactive
    # Commands to run in interactive sessions can go here
end

# Set the global editor variable for other programs to find.

set -gx EDITOR vim
