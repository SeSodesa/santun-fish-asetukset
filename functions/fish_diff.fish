function fish_diff -d "Runs diff in unified format on the input files."

	# Get function name.

	set -l fn_name (status current-function)

	# Generate options for argparse.

	set -f options (fish_opt --short h --long help)

	set options $options (fish_opt --short c --long color)

	set options $options (fish_opt --short s --long sort)

	# Generate option-related variables with argparse.

	argparse $options -- $argv

	# Handle options.

	if set -ql _flag_help
		echo "fish_diff [-h|--help, -c|--color, -s|--sort] file_1 file_2"
		return 0
	end

	if set -qf _flag_color
		set -f diff_color "--color"
	else
		set -f diff_color
	end

	if set -qf _flag_sort
		set -f cmd sort
	else
		set cmd cat
	end

	# Then read compulsory positional arguments and check for their correctness.

	set -l n_of_args (count $argv)

	if ! test "$n_of_args" -eq 2
		echo "$fn_name: the function needs 2 arguments: file1 and file2. Aborting…"
		return 1
	end

	set -l f1 $argv[1]

	if ! test -r $file1
		echo The given file 1 ($f1) does not exist or is not a file.
	end

	if ! test -r $file1
		echo The given file 2 ($f2) does not exist or is not a file.
	end

	set -l f2 $argv[2]

	diff $diff_color -u ($cmd $f1 | psub) ($cmd $f2 | psub)

end # function
