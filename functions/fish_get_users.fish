function fish_get_users -d "Reads regular users found on a system."

	set -f system (uname -s)

	if test $system = "Linux";

		awk -F'[/:]' '{if ($3 >= 1000 && $3 != 65534) print $1}' /etc/passwd

	else if test $system = "Darwin";

		dscacheutil -q user | ggrep -P "name: [^_]" - | awk '{print $2}'

	else;

		echo "Cannot get users for system of type $system."

	end

end
