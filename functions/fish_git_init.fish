function fish_git_init -d ""

# Get function name.

set -l fn_name (status current-function)

# Generate options for argparse.

set -f options (fish_opt --short h --long help)

set options $options (fish_opt --short p --long parents)

# Generate option-related variables with argparse.

argparse $options -- $argv

# Handle options.

if set -ql _flag_help
	echo "fish_git_init [-h|--help,-p|--parents] folder file_type_1 … file_type_n"
	return 0
end

if set -ql _flag_parents
	set -f mkdir_p p
else
	set -f mkdir_p
end

set -l folder $argv[1]

set -l filetypes $argv[2..]

# Genetate directory for repository.

mkdir $mkdir_p $folder

# Generate .gitignore file.

set -f gitignore_contents "### .gitignore
#
# This file determines which files are to be allowed into the project index by Git.
#

## Disallow everything.

*

## Allow README files in all folders.

!**/README
!**/README.md
!**/README.txt
!**/README.rst

## Allow specific files.

!.gitignore
!.gitattributes
!LICENSE

## Allow code files in src/.

!src/
"

for ft in $filetypes
	set gitignore_contents $gitignore_contents\n!src/\*.$ft
end

echo $gitignore_contents > $folder/.gitignore

# Generate a .gitattributes file.

set -f gitattributes_contents "### .gitattributes
#
# This file determines which files an file types are to be interpreted as plain text by Git.
# Plain text files are subject to normalization.
#

*.gitattributes  text
*.gitignore      text
*.md             text
LICENSE          text
"

for ft in $filetypes
	set gitattributes_contents $gitattributes_contents\n\*.$ft text
end

echo $gitattributes_contents > $folder/.gitattributes

# Generate README.md

echo "# $folder

This repository holds files related to **$folder**.

## License

See the attached [`LICENSE`](./LICENSE) file for your rights regarding the
content in this repository. Unless explicitly stated otherwise, all rights are
reserved.
" > $folder/README.md

# Generate LICENSE.

echo Copyright © (date +%Y) (whoami) > $folder/LICENSE

# Generate folder for source files.

mkdir $folder/src

echo \# src\n\nThis folder contains source code files for this project. > $folder/src/README.md

# Generate main file of main language.

set -l main_file_type $filetypes[-1]

touch $folder/src/main.$main_file_type

end # function
