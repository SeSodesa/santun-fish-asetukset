function fish_greeting -d "Defines the fish shell greeting message."
	echo "Päivää Santun vuonna "(date +%Y)". Olen fish v$FISH_VERSION. <>::"
end
