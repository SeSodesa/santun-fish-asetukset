function fish_gs --description 'Run gs on argv[2], saving it to argv[1].'

	set -l fn_name (status current-function)

	# Generate options for argparse.

	set -l options (fish_opt --short h --long help)

	set options $options (fish_opt --short p --long pdfacp --optional-val --long-only)

	# Parse arguments into local variables.

	argparse $options -- $argv

	if set -ql _flag_help
		echo "fish_gs [-h|--help, --pdfacp=0|1|2|3] output_file input_file"
		return 0
	end

	if not set -ql _flag_pdfacp
		set -f PDFACP 1
	else
		set -f PDFACP $_flag_pdfacp
	end

	if not contains "$PDFACP" 0 1 2 3
		echo "pdfacp=$PDFACP is not a valid value for GhostScript PDF/A compatibility policy. Must be in {0,1,2,3}."
		return 2
	end

	# Then read compulsory positional arguments and check for their correctness.

	set -l n_of_args (count $argv)

	if ! test "$n_of_args" -eq 2
		echo "$fn_name: the function needs 2 arguments: the target file and the input file. Aborting…"
		return 1
	end

	set -l target_file $argv[1]
	set -l source_file $argv[2]

	if ! test -f "$source_file"
		echo "$fn_name: Could not find input file $source_file. Aborting…"
		return 3
	end

	# Finally, run GhostScript on the inputs, if possible.

	if ! test -x (which gs)

		echo "$fn_name: GhostScript (gs) could not be found on this machine. Aborting…"

	end

	gs \
		-dCompatibilityLevel=1.7 \
		-dPDFA=3 \
		-dPDFACompatibilityPolicy="$PDFACP" \
		-sColorConversionStrategy=UseDeviceIndependentColor \
		-sDEVICE=pdfwrite \
		-dPDFSETTINGS=/prepress \
		-dNOPAUSE \
		-dQUIET \
		-dBATCH \
		-sOutputFile="$target_file" \
		"$source_file"
end
