function fish_prompt --description 'Write out the prompt'

	# Store value of status at the start.

	set -l mystatus $status

	# Color definitions

	set -l addresscolor (set_color --bold brblue)
	set -l usercolor (set_color --bold brgreen)
	set -l gitcolor (set_color --bold bryellow)
	set -l errcolor (set_color --bold brred)

	# The prompt itself

	printf '%s%s%s%s%s%s'\
		$usercolor$USER\
		$addresscolor'@'\
		$usercolor$hostname\
		$addresscolor'@'\
		$usercolor(pwd)\
		$gitcolor(fish_git_prompt)

	if ! test "$mystatus" -eq 0
		printf '\n%s' $errcolor'('$mystatus') '
	end

	if test "$mystatus" -eq 0
		printf '\n'
	end

	echo $addresscolor'λ '

end
