function fish_upgrade -d "Updates OS packages based on which package managers are available."

	switch (uname)

		case Linux

			if command -q apt

				sudo apt update && sudo apt upgrade -y

			end

			if command -q flatpak

				sudo flatpak upgrade -y

			end

			if command -q snap

				sudo snap refresh

			end

		case Darwin

			if command -q brew

				brew update && brew upgrade && brew upgrade --cask

			end

		case FreeBSD NetBSD DragonFly

				echo Updater function not yet implemented for BSD-based systems.

		case '*'

				echo Unknown OS. Not upgrading.

	end # switch

end # function
